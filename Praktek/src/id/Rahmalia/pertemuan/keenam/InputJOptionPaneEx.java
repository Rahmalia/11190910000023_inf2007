package id.Rahmalia.pertemuan.keenam;

import javax.swing.JOptionPane;


/**
 *
 * @author Rahmaliaputri
 */
public class InputJOptionPaneEx {
    public static void main(String[] args) {
        int bilangan;
        String box = JOptionPane.showInputDialog("Masukkan Bilangan: ");
        
        bilangan = Integer.parseInt(box);
        
        System.out.println("Bilangan: " + bilangan);
    }
}
