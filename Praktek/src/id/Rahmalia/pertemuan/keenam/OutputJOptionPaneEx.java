package id.Rahmalia.pertemuan.keenam;

import javax.swing.JOptionPane;

/**
 *
 * @author Rahmaliaputri
 */
public class OutputJOptionPaneEx {
    public static void main(String[] args) {
        int bilangan;
        String box = JOptionPane.showInputDialog("Masukkan Bilangan: ");
        
        bilangan = Integer.parseInt(box);
        
        JOptionPane.showMessageDialog(null, "Bilangan; " + bilangan, "Hasil Input", JOptionPane.INFORMATION_MESSAGE);

    }
}
