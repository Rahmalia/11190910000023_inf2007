package id.Rahmalia.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Rahmaliaputri
 */
public class CariMinimum {
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N, x, min, i;
        x = in.nextInt();
        N = in.nextInt();
        min = x;
        System.out.println("nilai yang akan dibandingkan ada "+ N);
        for (i = 2; i <= N; i++) {
            x = in.nextInt();
            if (x < min) {
                min = x;
            }
        }
        System.out.println(min + " adalah nilai paling minimum");
    }
}
