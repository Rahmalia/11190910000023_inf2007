package id.Rahmalia.pertemuan.kelima;
import java.util.Scanner;
/**
 *
 * @author Rahmaliaputri
 */
public class Perpangkatan {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        float a, p;
        int n,i;
        a = in.nextFloat();
        n = in.nextInt();
        p = 1;
        for (i = 1; i <= n; i++){
            p = p * a;
        }
        System.out.println(p);
    }
}
