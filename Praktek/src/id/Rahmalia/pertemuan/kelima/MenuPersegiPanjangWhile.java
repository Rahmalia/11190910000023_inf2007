package id.Rahmalia.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Rahmaliaputri
 */
public class MenuPersegiPanjangWhile {
    public static void main(String[] args) {
        int noMenu = 0;
        float panjang, lebar;
        float luas, keliling, diagonal;
        while (noMenu != 4) {
            System.out.println("Menu empat persegi panjang");
            System.out.println("1. Menghitung luas");
            System.out.println("2. Menghitung keliling ");
            System.out.println("3. Menghitung panjang diagonal");
            System.out.println("4. Keluar Program");
            System.out.println("Masukkan pilihan Anda (1/2/3/4)?");
            
            Scanner in = new Scanner(System.in);
            noMenu = in.nextInt();
            switch(noMenu) {
                case 1: {
                    panjang = in.nextFloat();
                    lebar = in.nextFloat();
                    luas = (panjang * lebar);
                    System.out.println("luas persegi panjang adalah " + luas);
                }
                    break;
                case 2: {
                    panjang = in.nextFloat();
                    lebar = in.nextFloat();
                    keliling = (2 * (panjang + lebar));
                    System.out.println("keliling persegi panjang adalah " + keliling);
                }
                    break;
                case 3: {
                    panjang = in.nextFloat();
                    lebar = in.nextFloat();
                    diagonal = ((panjang * panjang) + (lebar * lebar));
                    System.out.println("panjang diagonal "+ diagonal);
                }
                    break;
                case 4:
                    System.out.println("keluar program...sampai jumpa");
            }
            
        }
         }
    }
