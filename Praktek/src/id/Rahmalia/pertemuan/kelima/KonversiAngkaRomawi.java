package id.Rahmalia.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Rahmaliaputri
 */
public class KonversiAngkaRomawi {
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int angka = 0;
        while (angka != -1) {
            do{
                angka = in.nextInt();
                if (angka < -1 || angka > 500000000) {
                    System.out.println("error");
                } else {
                    while (angka >= 1000) {
                        System.out.println("M");
                        angka = angka - 1000;
                    }
                    if (angka >= 500) {
                        if (angka >= 900) {
                            System.out.println("CM");
                            angka = angka - 900;
                        } else {
                            System.out.println("D");
                            angka = angka - 500;
                        }
                    }
                    while (angka >= 100) {
                        if (angka >= 400) {
                            System.out.println("CD");
                            angka = angka - 400;
                        } else {
                            System.out.println("C");
                            angka = angka - 100;
                        }
                    }
                    if (angka >= 50) {
                        if (angka >= 90) {
                            System.out.println("XC");
                            angka = angka - 90;
                        } else {
                            System.out.println("L");
                            angka = angka - 50;
                        }
                    }
                    while (angka >= 10) {
                        if (angka >= 40) {
                            System.out.println("XL");
                            angka = angka - 40;
                        } else {
                            System.out.println("X");
                            angka = angka - 10;
                        }
                    }
                    if (angka >= 5) {
                        if (angka >= 9) {
                            System.out.println("IX");
                            angka = angka - 9;
                        } else {
                            System.out.println("V");
                            angka = angka - 5;
                        }
                    }
                    while (angka >= 1) {
                        if (angka >= 4) {
                            System.out.println("IV");
                            angka = angka - 4;
                        } else {
                            System.out.println("I");
                            angka = angka - 1;
                        }
                    }
                }
                System.out.println("");
            }while (angka == 1);
        }
    }
}
