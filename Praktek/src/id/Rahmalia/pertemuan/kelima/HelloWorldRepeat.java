package id.Rahmalia.pertemuan.kelima;

/**
 *
 * @author Rahmaliaputri
 */
public class HelloWorldRepeat {
    
    public static void main(String[] args) {
        int i = 1;
        do {
            System.out.println("Hello World");
            i = i + 1;
            
        } while (i < 10);
    }
}
