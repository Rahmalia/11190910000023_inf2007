package id.Rahmalia.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Rahmaliaputri
 */
public class PenjumlahanDeretPecahanWhile {
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x;
        float s = 0;
        x = in.nextInt();
        while (x != -1) {
            s = s + (float)1 / x;
            x = in.nextInt();
        }
        System.out.println(s);
    }
}
