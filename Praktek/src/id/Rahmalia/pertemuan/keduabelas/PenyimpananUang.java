package id.Rahmalia.pertemuan.keduabelas;

/**
 *
 * @author Rahmaliaputri
 */
public class PenyimpananUang extends Tabungan{
    private double tingkatBunga;

    public PenyimpananUang(int saldo,double tingkatBunga) {
        super(saldo);
        this.tingkatBunga=tingkatBunga;
    }
    
    public double cekUang (){
        this.saldo= (int) (saldo + (saldo*tingkatBunga));
        return this.saldo;
    }
}

    
