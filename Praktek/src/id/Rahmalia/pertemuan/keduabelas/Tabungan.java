package id.Rahmalia.pertemuan.keduabelas;

/**
 *
 * @author Rahmaliaputri
 */
public class Tabungan {
    protected int saldo;

    public Tabungan(int saldo) {
        this.saldo = saldo;
    }

    public int ambilUang(int jumlah) {
        saldo = saldo - jumlah;
        return this.saldo;
    }
}


