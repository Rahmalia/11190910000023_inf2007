package id.Rahmalia.pertemuan.kesebelas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rahmaliaputri
 */
public class ArsipTeks {
    int getHitungKarakter(FileReader T) {
        int n;
        char[] c;

        n = 0;
        Scanner line = new Scanner(new BufferedReader(T));
        while (line.hasNext()) {
            c = line.next().toCharArray();
            for (char d : c) {
                if (d == 'a') {
                    n = n + 1;
                }
            }
        }
        return n;
    }

    public static void main(String[] args) {
        ArsipTeks baca = new ArsipTeks();
        int a;
        try {
            a = baca.getHitungKarakter(new FileReader("D:\\Gitlab\\T.txt"));
            System.out.println(a);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ArsipTeks.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}


