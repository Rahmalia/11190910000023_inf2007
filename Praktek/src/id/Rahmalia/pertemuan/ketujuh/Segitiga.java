package id.Rahmalia.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Rahmaliaputri
 */
public class Segitiga {
    private float alas, tinggi, luas;
    Scanner in = new Scanner(System.in);
    public void Segitiga(){
        System.out.println("Masukkan nilai alas ");
        alas = in.nextFloat();
        System.out.println("Masukkan nilai tinggi ");
        tinggi = in.nextFloat();
        luas = (alas*tinggi)/2;
        System.out.println("Luas = " + luas);
    }
    
}
