package id.Rahmalia.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Rahmaliaputri
 */
public class AplikasiSegitigaParameter {
    public static void main(String[] args) {
        int i, N;
        float a = 0, t = 0;
        
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan banyaknya segitiga : ");
        N = in.nextInt();
        
        for (i = 0; i < N; i++) {
            System.out.println("Masukkan alas : ");
            a = in.nextFloat();
            System.out.println("Masukkan tinggi : ");
            t = in.nextFloat();
            SegitigaParameter hitungLuasSegitiga = new SegitigaParameter(a, t);
            hitungLuasSegitiga.getLuas();
        }
    }
}
