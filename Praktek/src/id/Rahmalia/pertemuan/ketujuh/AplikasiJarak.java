package id.Rahmalia.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Rahmaliaputri
 */
public class AplikasiJarak {
    public static void main(String[] args) {
        int x1, x2, y1, y2;
        Scanner in = new Scanner(System.in);
        System.out.println("Nilai x1 = ");
        x1 = in.nextInt();
        System.out.println("Nilai x2 = ");
        x2 = in.nextInt();
        System.out.println("Nilai y1 = ");
        y1 = in.nextInt();
        System.out.println("Nilai y2 = ");
        y2 = in.nextInt();
        
        Jarak hitungJarak = new Jarak();
        System.out.println("Jarak titik 1 dengan titik 2 : " + hitungJarak.Jarak(x1, x2, y1, y2));
    }
}
