package id.Rahmalia.pertemuan.ketiga;

/**
 *
 * @author Rahmaliaputri
 */
import java.util.Scanner;

public class Tukaran_Pecahan {
    
    public static void main(String[] args) {
        int jumlahUang, pecahan1, pecahan2, pecahan3, pecahan4, pecahan5, sisa;
        Scanner in = new Scanner(System.in);
        System.out.println("jumlah uang : ");
        jumlahUang = in.nextInt();
        
        pecahan1 = jumlahUang / 1000;
        sisa = jumlahUang % 1000;
        pecahan2 = jumlahUang / 500;
        sisa = jumlahUang % 500;
        pecahan3 = jumlahUang / 100;
        sisa = jumlahUang % 100;
        pecahan4 = jumlahUang / 50;
        sisa = jumlahUang % 50; 
        pecahan5 = jumlahUang / 25;
        
        System.out.println("tukaran pecahannya");
        System.out.println("Rp. 1000 = " + pecahan1 + " buah");
        System.out.println("Rp. 500 = " + pecahan2 + " buah");
        System.out.println("Rp. 100 = " + pecahan3 + " buah");
        System.out.println("Rp. 50 = " + pecahan4 + " buah");
        System.out.println("Rp. 25 = " + pecahan5 + " buah"); 
    }
}
