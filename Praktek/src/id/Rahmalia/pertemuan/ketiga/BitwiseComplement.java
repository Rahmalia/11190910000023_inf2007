package id.Rahmalia.pertemuan.ketiga;

/**
 *
 * @author Rahmaliaputri
 */
public class BitwiseComplement {
    public static void main(String[] args) {
        int x = 8;
        System.out.println("x = " + x);
        int y = ~x;
        System.out.println("y = " + y);
    }
}
