package id.Rahmalia.pertemuan.ketiga;

/**
 *
 * @author Rahmaliaputri
 */
public class Bitwise {
    public static void main(String[] args) {
        int x = 5, y = 6;
        System.out.println("x = " + x);
        System.out.println("y = " + y);
        System.out.println("x & y = " + (x & y));
        System.out.println("x | y = " + (x | y));
        System.out.println("x ^ y = " + (x ^ y));
    }
}
