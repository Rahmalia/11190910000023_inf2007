package id.Rahmalia.pertemuan.ketiga;

/**
 *
 * @author Rahmaliaputri
 */
public class BooleanOr {
    public static void main(String[] args) {
        int a = 5, b = 7;
        if ((a > 2) | (b++ < 10)) {
            b += 2;
        }
        System.out.println(b);
    }
}
