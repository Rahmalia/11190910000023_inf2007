package id.Rahmalia.pertemuan.ketiga;

/**
 *
 * @author Rahmaliaputri
 */
import java.util.Scanner;

public class Selisih_Dua_Tanggal {
    
    public static void main(String[] args) {
        int tanggal1, bulan1, tahun1, tanggal2, bulan2, tahun2, tanggal3, bulan3, tahun3, selisih;
        Scanner ln = new Scanner(System.in);
        System.out.println("tanggal 1 : ");
        tanggal1 = ln.nextInt();
        System.out.println("bulan 1 : ");
        bulan1 = ln.nextInt();
        System.out.println("tahun 1 : ");
        tahun1 = ln.nextInt();
        System.out.println("tanggal 2 : ");
        tanggal2 = ln.nextInt();
        System.out.println("bulan 2 : ");
        bulan2 = ln.nextInt();
        System.out.println("tahun 2 : ");
        tahun2 = ln.nextInt();
        
        selisih = (tahun2 - tahun1) * 365 + (bulan2 - bulan1) * 30 + (tanggal2 - tanggal1);
        tahun3 = selisih / 365;
        bulan3 = (selisih % 365) / 30;
        tanggal3 = (selisih % 365) % 30;
        
        System.out.println("selisih");
        System.out.println(tahun3 + " tahun " + bulan3 + " bulan " + tanggal3 + " hari ");
    }
}
 
