package id.Rahmalia.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author Rahmaliaputri
 */
public class BilanganGenap {
    public static void main(String[] args) {
        Scanner in= new Scanner(System.in);
        int angka = 2;
        if (angka % 2 == 0)
            System.out.println("Angka "+ angka +" adalah bilangan GENAP");
    }
}
