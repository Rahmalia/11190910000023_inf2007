package id.Rahmalia.SalonRambut.Model;

/**
 *
 * @author Rahmaliaputri
 */
public class Info {

    private final String aplikasi = "Aplikasi Salon Rambut Sederhana";
    private final String version = "Versi 1.0.0";

    public String getAplikasi() {
        return aplikasi;
    }

    public String getVersion() {
        return version;
    }
}
