package id.Rahmalia.SalonRambut.Model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * @author ASUS
 */
public class SalonRambut implements Serializable {

    private int jenis;
    private int gender;
    private LocalDateTime waktuMasuk;
    private BigDecimal biaya;
    private boolean keluar = false;

    public SalonRambut(){

    }

    public SalonRambut(int jenis, int gender, LocalDateTime waktuMasuk, BigDecimal biaya) {
        this.jenis = jenis;
        this.gender = gender;
        this.waktuMasuk = waktuMasuk;
        this.biaya = biaya;
    }

    public int getJenis() {
        return jenis;
    }

    public void setJenis(int jenis) {
        this.jenis = jenis;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public LocalDateTime getWaktuMasuk() {
        return waktuMasuk;
    }

    public void setWaktuMasuk(LocalDateTime waktuMasuk) {
        this.waktuMasuk = waktuMasuk;
    }

    public BigDecimal getBiaya() {
        return biaya;
    }

    public void setBiaya(BigDecimal biaya) {
        this.biaya = biaya;
    }

    public boolean isKeluar() {
        return keluar;
    }

    public void setKeluar(boolean keluar) {
        this.keluar = keluar;
    }
    
    @Override
    public String toString() {
        return "SalonRambut{" + "Gender=" + gender + ", Jenis=" + jenis + ", waktuMasuk=" + waktuMasuk + ", biaya=" + biaya + ", keluar=" + keluar + '}';
    }
}
