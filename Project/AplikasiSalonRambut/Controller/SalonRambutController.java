package id.Rahmalia.SalonRambut.Controller;
import id.Rahmalia.SalonRambut.Model.SalonRambut;
import com.google.gson.Gson;
import id.Rahmalia.SalonRambut.Model.SalonRambut;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author ASUS
 */
public class SalonRambutController {
    private static final String FILE = "D:\\SalonRambut.json";
    private SalonRambut salonrambut;
    private int jenis;
    private int gender;
    private LocalDateTime waktuMasuk;
    private BigDecimal biaya;
    private boolean keluar = false;
    private final DateTimeFormatter dateTimeFormat;
    private int pilihan;
    private final Scanner in;
    
    public SalonRambutController() {
        in = new Scanner(System.in);
        waktuMasuk = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    }
    
    public void setMasukSalonRambut() {
        System.out.println("Gender Pelanggan 1=Laki-laki, 2=Perempuan ");
        System.out.print("Masukkan Gender Pelanggan : ");
        while (!in.hasNextInt()) {
            String input = in.next();
            System.out.printf("\"%s\" is not a valid number.\n", input);
            System.out.print("Masukkan Gender Pelanggan : ");
        }
        gender = in.nextInt();

        if (gender==1){
            System.out.println("Jenis Potongan 1= Potong Rambut, 2=Cukur Jenggot, 3=Cukur Kumis,  ");
            System.out.print("Masukkan Jenis Potongan : ");
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" is not a valid number.\n", input);
                System.out.print("Masukkan Jenis Potongan : ");
            }
            jenis = in.nextInt();
        }else {
            System.out.println("Jenis Potongan 1= Potong Rambut, 2=creambath ");
            System.out.print("Masukkan Jenis Potongan : ");
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" is not a valid number.\n", input);
                System.out.print("Masukkan Jenis Potongan : ");
            }
            jenis = in.nextInt();
        }
            

        if ((gender == 1) && (jenis == 1)) {
            biaya = new BigDecimal(25000);
        } else if ((gender == 1) && (jenis == 2)) {
            biaya = new BigDecimal(10000);
        } else if ((gender == 1) && (jenis == 3)) {
            biaya = new BigDecimal(5000);
        } else if ((gender == 2) && (jenis == 1)) {
            biaya = new BigDecimal(35000);
        } else if ((gender == 2) && (jenis == 2)) {
            biaya = new BigDecimal(60000);
        } 
        System.out.println(" biaya = " + biaya);
       

        String formatWaktuMasuk = waktuMasuk.format(dateTimeFormat);
        System.out.println("Waktu Masuk : " + formatWaktuMasuk);

        salonrambut = new SalonRambut();
        salonrambut.setGender(gender);
        salonrambut.setJenis(jenis);
        salonrambut.setWaktuMasuk(waktuMasuk);
        salonrambut.setBiaya(biaya);
        salonrambut.setKeluar(true);
        setWriteSalonRambut(FILE, salonrambut);

        System.out.println("Apakah mau Input kembali?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setMasukSalonRambut();
        }
    }
    
    public List<SalonRambut> getReadSalonRambut(String file) {
        List<SalonRambut> salonrambuts = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try ( Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                SalonRambut[] s = gson.fromJson(line, SalonRambut[].class);
                salonrambuts.addAll(Arrays.asList(s));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SalonRambutController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SalonRambutController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return salonrambuts;
    }
    
    public void setWriteSalonRambut(String file, SalonRambut salonrambut) {
        Gson gson = new Gson();

        List<SalonRambut> salonrambuts = getReadSalonRambut(file);
        salonrambuts.remove(salonrambut);
        salonrambuts.add(salonrambut);

        String json = gson.toJson(salonrambuts);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(SalonRambutController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void getDataSalonRambut() {
        List<SalonRambut> SalonRambuts = getReadSalonRambut(FILE);
        Predicate<SalonRambut> isKeluar = e -> e.isKeluar() == true;
        Predicate<SalonRambut> isDate = e -> e.getWaktuMasuk().toLocalDate().equals(LocalDate.now());

        List<SalonRambut> sResults = SalonRambuts.stream().filter(isKeluar.and(isDate)).collect(Collectors.toList());
        BigDecimal total = sResults.stream()
                .map(SalonRambut::getBiaya)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("Jenis Potongan Rambut \tGender \t\tWaktu Masuk \t\tBiaya");
        System.out.println("--------- \t----------- \t\t------------ \t\t-----");
        sResults.forEach((s) -> {
            System.out.println(s.getGender()+ "\t\t" + s.getWaktuMasuk().format(dateTimeFormat) + "\t"  + "\t" + s.getBiaya());
        });
        System.out.println("--------- \t----------- \t\t------------ \t\t-----");
        System.out.println("====================================");
        System.out.println("Pendapatan Total = Rp. " + total);
        System.out.println("====================================");

        System.out.println("Apakah mau mengulanginya?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            getDataSalonRambut();
        }
    }
    
}
